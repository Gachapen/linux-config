#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

export HISTSIZE=10000
export HISTFILESIZE=10000
export HISTCONTROL=ignoredups:erasedups
export MAKEFLAGS="-j$(nproc)"

# When the shell exits, append to the history file instead of overwriting it
shopt -s histappend

# After each command, append to the history file and reread it
export PROMPT_COMMAND="${PROMPT_COMMAND:+$PROMPT_COMMAND$'\n'}history -a; history -c; history -r"

alias ls='ls --color=auto'
alias ll='ls -lFh'
alias grep='grep --color=auto -n'
alias tp='trash-put'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias open='xdg-open'

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
	. /etc/bash_completion
fi

export ANDROID_SDK=~/coding/sdk/android-sdk
export ANDROID_HOME=$ANDROID_SDK
export ANDROID_NDK=~/coding/sdk/android-ndk
#export PATH="$HOME/bin:$PATH"
export PATH="${ANDROID_SDK}/tools:${ANDROID_SDK}/platform-tools:${ANDROID_SDK}/build-tools/24.0.0/:$PATH"
#export PATH="$HOME/.cargo/bin/:$PATH"
export WINEARCH="win32"
export EMSCRIPTEN="/usr/share/emscripten"

if [ -d /usr/src/rust ]; then
	export RUST_SRC_PATH=/usr/src/rust/src/
elif [ -d /usr/local/src/rust ]; then
	export RUST_SRC_PATH=/usr/local/src/rust/src/
else
	export RUST_SRC_PATH=$HOME/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/lib/rustlib/src/rust/src/
fi

PS1_START="\[\e[00;32m\][\[\e[00;33m\]\u@\h\[\e[00;32m\]][\[\e[00;33m\]\T\[\e[00;32m\]][\[\e[00;33m\]\w\[\e[00;32m\]]\[\e[00;37m\]\\[\e[0m\]"
PS1_END="\n> "

if [ -f ~/.bash-git-prompt/gitprompt.sh ]; then
	GIT_PROMPT_THEME=Solarized
	GIT_PROMPT_FETCH_REMOTE_STATUS=0
	GIT_PROMPT_SHOW_UPSTREAM=1
	GIT_PROMPT_SHOW_UNTRACKED_FILES=normal
	GIT_PROMPT_START=$PS1_START
	GIT_PROMPT_END=$PS1_END
	source ~/.bash-git-prompt/gitprompt.sh
else
	export PS1=$PS1_START$PS1_END
fi

export NVM_DIR="/home/magnus/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm

export XDG_CONFIG_HOME=$HOME/.config
