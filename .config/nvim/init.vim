if &compatible
  set nocompatible
endif

" Dein
let g:dein#install_progress_type = 'tabline'
set runtimepath^=~/.vim/bundle/repos/github.com/Shougo/dein.vim

call dein#begin('~/.vim/bundle')

" Default plugins
call dein#add('Shougo/dein.vim')
call dein#add('Shougo/neocomplete.vim')

" Basic setup.
call dein#add('tpope/vim-sensible')

" Themes
call dein#add('altercation/vim-colors-solarized')

" Syntax checking
call dein#add('vim-syntastic/syntastic')

" HTML/CSS snippets
call dein#add('mattn/emmet-vim')

" Match HTML/XML tags
call dein#add('Valloric/MatchTagAlways')

" SASS/SCSS syntax
call dein#add('cakebaker/scss-syntax.vim')

" TypeScript
call dein#add('Shougo/vimproc.vim', {'build' : 'make'})
call dein#add('leafgarland/typescript-vim')
call dein#add('Quramy/tsuquyomi')

" C++ highlighting
call dein#add('octol/vim-cpp-enhanced-highlight')

" C++ switch between header and source
call dein#add('vim-scripts/FSwitch')

" C++ completion
call dein#add('Valloric/YouCompleteMe', {'build' : 'git submodule update --init --recursive && python install.py --clang-completer --racer-completer'})

" C++ jump to symbols with ctags.
call dein#add('majutsushi/tagbar')

" File explorer
call dein#add('scrooloose/nerdtree')

" Improved whitespace detection and removal.
call dein#add('ntpeters/vim-better-whitespace')

" Nicer statusbar.
call dein#add('vim-airline/vim-airline')

" Support for Markdown language.
call dein#add('godlygeek/tabular')
call dein#add('plasticboy/vim-markdown')

" Better JSON support.
call dein#add('elzr/vim-json')

" Show git diff in the sign column.
call dein#add('airblade/vim-gitgutter')

" Open files by searching for them.
call dein#add('wincent/Command-T', {'build' : 'cd ruby/command-t/ext/command-t && { make clean; ruby extconf.rb && make }'})

" Easier commenting.
call dein#add('scrooloose/nerdcommenter')

" Python stuff
call dein#add('klen/python-mode')

" Mardown heading folding
call dein#add('nelstrom/vim-markdown-folding')

" Gradle support
call dein#add('tfnico/vim-gradle')

" Saving vim session
call dein#add('xolox/vim-misc')
call dein#add('xolox/vim-session')

" Global grep and replace
call dein#add('dkprice/vim-easygrep')

" Rust programming language support
call dein#add('rust-lang/rust.vim')

" Use asterisk to select visual parts
call dein#add('haya14busa/vim-asterisk')

" Auto detect indentation
call dein#add('tpope/vim-sleuth')

" Toggle between absolute and relative numbering
call dein#add('jeffkreeftmeijer/vim-numbertoggle')

" Clang-format
call dein#add('rhysd/vim-clang-format')

" Git in Vim
call dein#add('tpope/vim-fugitive')

" toml syntax support
call dein#add('cespare/vim-toml')

" Linting and make framework
call dein#add('neomake/neomake')

" run commands in one terminal
call dein#add('kassio/neoterm')

call dein#end()

source $XDG_CONFIG_HOME/vim/common.vim

set mouse=a
