syntax on
filetype plugin indent on

" Store undo history offline
set undofile
set undodir=$XDG_CONFIG_HOME/vim/undofile

" Confirm before quit rather than complain
set confirm

" Disable swapfiles.
set noswapfile

" Always show status line
"set laststatus=2

set background=dark
colorscheme solarized

" Highlight search results
"set hlsearch

" Show line numbers
set number

" Enable mouse support
"set mouse=a

" Disable scratch preview window
"set completeopt-=preview

" Allow sourcing .vimrc files
set exrc

" Soft wrap lines
set wrap

autocmd Filetype java setlocal ts=4 sw=4 expandtab
autocmd Filetype yml setlocal ts=2 sw=2 expandtab
autocmd Filetype html setlocal ts=2 sw=2 expandtab
autocmd Filetype py setlocal ts=4 sw=4 expandtab
autocmd Filetype sass setlocal ts=2 sw=2 expandtab
autocmd Filetype rs setlocal ts=4 sw=4 expandtab
autocmd Filetype toml setlocal ts=4 sw=4 expandtab

" YouCompleteMe config
let g:ycm_global_ycm_extra_conf = "~/.vim/ycm_extra_conf.py"
let g:ycm_extra_conf_globlist = ['~/coding/suttung/*','~/coding/personal/*']
let g:ycm_rust_src_path = $RUST_SRC_PATH
let g:ycm_always_populate_location_list = 1
let g:ycm_server_python_interpreter = "python"

let g:session_autosave = 'no'
let g:session_autoload = 'no'

" vim-markdown config
let g:vim_markdown_folding_disabled=1

" vim-racer config
"set hidden
let g:racer_cmd = "racer"
let g:racer_experimental_completer = 1

" vim-asterisk config
map *   <Plug>(asterisk-*)
map #   <Plug>(asterisk-#)
map g*  <Plug>(asterisk-g*)
map g#  <Plug>(asterisk-g#)
map z*  <Plug>(asterisk-z*)
map gz* <Plug>(asterisk-gz*)
map z#  <Plug>(asterisk-z#)
map gz# <Plug>(asterisk-gz#)

" NERDTree config
"autocmd VimEnter * NERDTree
autocmd BufEnter * NERDTreeMirror
"CTRL-t to toggle tree view with CTRL-t
nmap <silent> <C-t> :NERDTreeToggle<CR>
"Set F2 to put the cursor to the nerdtree
nmap <silent> <F2> :NERDTreeFind<CR>

" airline
let g:airline_powerline_fonts = 1

" Strip whitespace on save
autocmd BufWritePre * StripWhitespace
autocmd! BufWritePost *.rs Neomake! clippy

" Have Vim jump to the last position when reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
    \| exe "normal! g'\"" | endif
endif

" Function to diff with saved file. Run DiffSaved.
function! s:DiffWithSaved()
  let filetype=&ft
  diffthis
  vnew | r # | normal! 1Gdd
  diffthis
  exe "setlocal bt=nofile bh=wipe nobl noswf ro ft=" . filetype
endfunction
com! DiffSaved call s:DiffWithSaved()
