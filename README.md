# My own linux config (dotfiles)

## Install
Clone this repository into your home folder:
```
git init
git remote add origin https://gitlab.com/Gachapen/linux-config.git
git fetch
git checkout master
git submodule update --init
```

Install any dependencies:
* i3
 * i3blocks - the status bar used instead of i3status.
 * acpi - for battery status in status bar.
 * sysstat - for CPU usage in status bar.
 * alsa-utils - for volume status and control in status bar.
 * dex-git - autostart applications from ~/.config/autostart.
 * dmenu - application launcher.
 * xkb-switch - switch keyboard layouts.
 * ttf-inconsolata - font used in UI.
 * python-fontawesome - for autorename_workspaces.sh
 * otf-font-awesome - for autorename_workspaces.sh
 * python-i3ipc - for autorename_workspaces.sh
 * pasystray - for volume control in systray
* zsh
 * zsh - the shell itself.
 * oh-my-zsh - zsh plugins and themes.
 * powerline-fonts - fonts used in terminal.

Arch Linux two-liner:
```
trizen -S i3blocks acpi sysstat alsa-utils dex-git dmenu xkb-switch-git ttf-inconsolata zsh oh-my-zsh-git powerline-fonts python-pip otf-font-awesome pasystray
sudo pip install fontawesome i3ipc
```

Install Vim plugins:
1. Open ~/.vimrc with Vim.
2. Run `:call dein#install()`.
