# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000
setopt appendhistory autocd extendedglob nomatch
unsetopt beep
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/magnus/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# Oh My Zsh

# Path to your oh-my-zsh installation.
ZSH=/usr/share/oh-my-zsh/

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="agnoster"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git)

ZSH_CACHE_DIR=$HOME/.oh-my-zsh-cache
if [[ ! -d $ZSH_CACHE_DIR ]]; then
  mkdir $ZSH_CACHE_DIR
fi

source $ZSH/oh-my-zsh.sh


# Custom config

export MAKEFLAGS="-j$(nproc)"
export ANDROID_SDK=~/coding/sdk/android-sdk
export ANDROID_HOME=$ANDROID_SDK
export ANDROID_NDK=~/coding/sdk/android-ndk
export PATH="${ANDROID_SDK}/tools:${ANDROID_SDK}/platform-tools:${ANDROID_SDK}/build-tools/24.0.0/:$PATH"
export WINEARCH="win32"
export EMSCRIPTEN="/usr/share/emscripten"
export RUST_SRC_PATH=$HOME/.rustup/toolchains/$(rustup toolchain list | grep default | awk '{print $1}')/lib/rustlib/src/rust/src/
export XDG_CONFIG_HOME=$HOME/.config

#PS1_START="\[\e[00;32m\][\[\e[00;33m\]\u@\h\[\e[00;32m\]][\[\e[00;33m\]\T\[\e[00;32m\]][\[\e[00;33m\]\w\[\e[00;32m\]]\[\e[00;37m\]\\[\e[0m\]"
#PS1_END="\n> "

#export PS1=$PS1_START$PS1_END

#export PROMPT='%F{green}[%F{yellow}%n@%m%F{green}][%F{yellow}%D{%H:%M:%S.%.}%F{green}][%F{yellow}%~%F{green}]%f
#%# '

alias ls='ls --color=auto'
alias ll='ls -lFh'
alias grep='grep --color=auto'
alias grp='grep --color=auto -n'
alias tp='trash-put'
alias open='xdg-open'
